function createDisplayMatrix(n) {
	var rows = (2 * n) - 1;
	var columns = (2 * n) - 1;

	var mx = [];

	for ( var i = 0; i < columns; i++ ) {
		mx[i] = [];

		for ( var j = 0; j < rows; j++ )
			mx[i][j] = getRandomInt(0,9);

		write(mx[i].toString() + '<br>');
	}

	return mx;
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * ((max + 1) - min)) + min;
}

function write(text) {
	document.body.innerHTML += text;
}

function spiralOutput(mx) {
	var n = mx.length;

	var center = parseInt(n / 2);

	// [column][row]
	for ( var steps = 1; steps <= center; steps++ ) {
		if ( steps === 1 ) {
			write('Start<br>');
			write(mx[center][center]);
			write(mx[center][center - steps]);
		}
		else if ( steps > 1 ) {
			write('<br><br>Down ' + (steps - 1) + '<br>');
			mx.forEach( function(el, i) {
				if ( i >= center - steps + 1 && i <= center + steps - 1 ) {
					// console.log(el[center - steps]);
					write(el[center - steps]);
				}
			} );
		}

		write('<br><br>Right ' + (steps) + '<br>');
		mx[center + steps].forEach( function(el, i) {
			if ( i >= center - steps && i <= center + steps - 1 ) {
				write(el);
			}
		} );

		write('<br><br>Up ' + (steps) + '<br>');
		mx.reverse().forEach( function(el, i) {
			if ( i >= center - steps && i <= center + steps - 1 ) {
				write(el[center + steps]);
			}
		} );
		mx.reverse();

		write('<br><br>Left ' + (steps) + '<br>');
		mx[center - steps].reverse().forEach( function(el, i) {
			if ( i >= center - steps && i <= center + steps ) {
				write(el);
			}
		} );
		mx[center - steps].reverse();
	}
}

write('Input matrix:<br><br>');

var mx = createDisplayMatrix(5);

write('<br>Spiral matrix from input:<br><br>');

spiralOutput(mx);